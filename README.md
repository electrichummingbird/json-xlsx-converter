#JSON xlsx converter

## instalation
Until we have our npm repos we will be instaling from bitbucket

    npm install git+ssh://git@bitbucket.org:electrichummingbird/json-xlsx-converter.git

## usage

    var jsonXlsxConverter = require('json-xlsx-converter'),
        converter = new jsonXlsxConverter();

    // convert xlsx file
    converter.readXlsxFile('test.xlsx').then(function (self) {
        console.log(self.convertToJson().json);
    }).fail(function (err) {
        console.log(err);
    });

    // convert JSON object
    converter.json = [{
        country: 'en',
        XX: {
            test: 1,
            tok: 'sdas',
            asdsad: 'sdfds'
        }
    }, {
        country: 'en',
        XX: {
            test: 1,
            tok: 'sdas',
            asdsad: 'sdfds'
        }
    }];
    converter.convertToXlsx().saveXlsxFile('test1.xlsx');
